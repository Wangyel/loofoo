const mongoose = require("mongoose")

const feedbackSchema = new mongoose.Schema({
    userName:{
        type:String,
        required:true,
        trim:true,
        lowercase:true,
    },
    email:{
        type:String,
        required:true,
        trim:true,
        lowercase:true,
    },
    body:{
        type:String,
        required:true
    }
})

const feedback = mongoose.model("feedback",feedbackSchema)

module.exports = feedback