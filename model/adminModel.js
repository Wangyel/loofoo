const mongoose = require("mongoose")
const bcrypt = require("bcryptjs")
const jwt = require("jsonwebtoken") // creating token
const user = require("./userModel")

require("dotenv").config({ path: './configuration/config.env'}) 

const adminSchema = new mongoose.Schema({
    userName: {
        type: String,
        required: true,
    },
    password:{
        type: String,
        required:true,
        trim: true,
        minlength:8,
    },
    tokens:[{
        token:{
            type:String,
            require:true
        }
    }]
})

adminSchema.pre("save", async function(next){
    const admin = this

    if(admin.isModified("password")) {
        admin.password = await bcrypt.hash(admin.password,8)
    }
    next()
})

adminSchema.methods.generateAuthToken = async function (){
    const admin = this // refering to a particular user data
    const token = jwt.sign({_id:admin._id.toString()}, process.env.TOKEN_SIGNATURE ) // to fetch the data from config.env file
    
    admin.tokens = admin.tokens.concat({token:token}) // stores the token in database
    await admin.save() // save to database

    return token
}

adminSchema.statics.findByCredentials = async (email, password) => { // to check if the user data is ther in database or not
    const admins = await admin.findOne({email:email}) // in user tbale find one row. check email in email field

    if(!admins){
        throw "Invalid Credential"
    }

    const isMatch = await bcrypt.compare(password,admins.password)
    if (!isMatch) {
        throw "Invalid Credential"
    }

    return admins
}

const admin = mongoose.model("admin", adminSchema)
module.exports = admin