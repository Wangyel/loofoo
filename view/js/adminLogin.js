+ function($) {
    $('.palceholder').click(function() {
      $(this).siblings('input').focus();
    });
  
    $('.form-control').focus(function() {
      $(this).parent().addClass("focused");
    });
  
    $('.form-control').blur(function() {
      var $this = $(this);
      if ($this.val().length == 0)
        $(this).parent().removeClass("focused");
    });
    $('.form-control').blur();
  
    // validetion
    $.validator.setDefaults({
      errorElement: 'span',
      errorClass: 'validate-tooltip'
    });
  
    $("#formvalidate").validate({
      rules: {
        email: {
          required: true,
        },
        userPassword: {
          required: true,
          minlength: 8
        }
      },
      messages: {
        email: {
          required: "Please enter your username."
        },
        userPassword: {
          required: "Enter your password.",
          minlength: "Incorrect login or password."
        }
      }
    });
  
  }(jQuery);
  document.getElementById("formvalidate").addEventListener("submit", function(event) {
    event.preventDefault(); // Prevent form submission
  
    // Get form field values
  const userName = document.getElementById("email").value;
  const password = document.getElementById("password").value;
  
  const adminData = {
    userName: userName,
    password: password
  };
  
  console.log(adminData)
  
  fetch("/admin/login", {
    method: "POST",
    headers: {"Content-Type": "application/json; charset=UTF-8" },
    body: JSON.stringify(adminData)
  })
  .then((res) =>  {
    if (res.ok) {
      window.open("adminhome.html")
      
      // Handle the success response from the server
  } else {
      throw new Error("Error logging admin");
  }
  })
  .catch((error) => {
    alert(error);
  });
  });