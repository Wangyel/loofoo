

window.onload = () => {
    fetch("/items/found")
        .then(res => res.json())
        .then((data) => {
            data.forEach(items => {
                console.log("item ", items);
                const parent = document.querySelector(".parent").cloneNode(true)
                const date = parent.querySelector('#date');
                const time = parent.querySelector('#time');
                const locationElement = parent.querySelector('#location1');
                const owner = parent.querySelector('#owner');
                const contact = parent.querySelector('#contact');
                const designation = parent.querySelector('#designation');
                const description = parent.querySelector('#description');
                const avatar = parent.querySelector('#image');
                const claim = parent.querySelector("#claim")

                date.innerHTML = items.date;
                time.innerHTML = convertTo12HourFormat(items.time);
                locationElement.innerHTML = items.location;
                owner.innerHTML = items.owner;
                contact.innerHTML = items.contact;
                designation.innerHTML = items.designation;
                description.innerHTML = items.description;
                avatar.src = `http://localhost:3000/item/${items._id}/avatar`;
                claim.href = `mailto:${items.email}`;

                const p = document.querySelector(".parent")
                p.insertAdjacentElement('afterend', parent)
            });
            document.querySelector(".parent").remove()
            console.log(data);
        })
        .catch(e => console.log(e));
}
function convertTo12HourFormat(time24) {
    var timeParts = time24.split(':');
    var hours = parseInt(timeParts[0]);
    var minutes = parseInt(timeParts[1]);
  
    var period = hours >= 12 ? 'PM' : 'AM';
  
    // Convert hours to 12-hour format
    if (hours > 12) {
      hours -= 12;
    } else if (hours === 0) {
      hours = 12;
    }
  
    // Format minutes to have leading zero if necessary
    var formattedMinutes = (minutes < 10) ? '0' + minutes : minutes;
  
    return hours + ':' + formattedMinutes + ' ' + period;
  }