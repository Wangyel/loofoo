window.onload = () => {
    fetch("/feedback")
        .then(res => res.json())
        .then((data) => {
            console.log("data ", data);
            data.message.forEach(items => {
                const parent = document.querySelector(".parent").cloneNode(true)
                const email = parent.querySelector('#email');
                const message = parent.querySelector('#message');

                email.innerHTML = items.email;
                message.innerHTML = items.body;

                const p = document.querySelector(".parent")
                p.insertAdjacentElement('afterend', parent)
            });
            document.querySelector(".parent").remove()
            console.log(data);
        })
        .catch(e => console.log(e));
}
// window.onload = () => {
//     fetch("/feedback")
//       .then(response => response.json())
//       .then(data => {
//         data.forEach(feedback => {
//           const parent = document.getElementById("parent").cloneNode(true);
//           parent.removeAttribute("id");
//           parent.querySelector("#email").textContent = `Email: ${feedback.email}`;
//           parent.querySelector("#message").textContent = `Message: ${feedback.message}`;
  
//           const mainCardBox = document.querySelector(".main-card-box");
//           mainCardBox.appendChild(parent);
//         });
//       })
//       .catch(error => {
//         console.error('Error:', error);
//       });
//   };

  
  
  
  
  
  