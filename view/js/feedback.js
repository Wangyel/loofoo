document.getElementById("formvalidate").addEventListener("submit", function(event) {
    event.preventDefault(); // Prevent form submission

var userName = document.getElementById('name').value
var email = document.getElementById('email').value
var body = document.getElementById('message').value

const data = {
    userName:userName,
    email:email,
    body:body
}

console.log(data)

fetch("/feedback/create",{
    method:"POST",
    headers:{'Content-Type':'application/json;charset=UTF-8'},
    body:JSON.stringify(data)
})
.then((res)=> {
    if (res.ok){
        alert("Feedback send successfully")
        resetform()
        return
    }
    else{
        throw ""
    }
}).catch((e) => {console.log(e)})
})