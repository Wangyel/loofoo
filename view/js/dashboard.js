const totalUsers = document.getElementById("totalUsers")
const totalLost = document.getElementById("lostItems")
const totalFeedback = document.getElementById("totalFeedback")
const totalFound = document.getElementById("foundItems")

window.onload = () => {
    fetch("/stats")
        .then(res => res.json())
        .then(data => {
            totalUsers.innerHTML = data.message.totalUser;
            totalFeedback.innerHTML = data.message.totalFeedback;
            totalLost.innerHTML = data.message.totalLost;
            totalFound.innerHTML = data.message.totalFound
        })
        .catch(e => console.log(e))
}
