
window.onload = () => {
    fetch("/items/lost")
        .then(res => res.json())
        .then((data) => {
            data.forEach(items => {
                console.log("item ", items);
                const parent = document.querySelector(".carousel__slide").cloneNode(true)
                const date = parent.querySelector('#date');
                const time = parent.querySelector('#time');
                const locationElement = parent.querySelector('#location1');
                const owner = parent.querySelector('#owner');
                const contact = parent.querySelector('#contact');
                const designation = parent.querySelector('#designation');
                const description = parent.querySelector('#description');
                const avatar = parent.querySelector('.avatar');
                const report = parent.querySelector("#report")

                date.innerHTML = items.date;
                time.innerHTML = items.time;
                locationElement.innerHTML = items.location;
                owner.innerHTML = items.owner;
                contact.innerHTML = items.contact;
                designation.innerHTML = items.designation;
                description.innerHTML = items.description;
                avatar.src = `http://localhost:3000/item/${items._id}/avatar`;
                report.href = `mailto:${items.email}`;

                const p = document.querySelector(".carousel__slide")
                p.insertAdjacentElement('afterend', parent)
            });
            document.querySelector(".carousel__slide").remove()
            console.log(data);
        })
        .catch(e => console.log(e));
}
