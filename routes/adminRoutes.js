const express = require("express")
const router = express.Router()// set up the router
const auth = require("../middleWare/auth")


const controller = require("../controller/admin")

router.post("/admin/signup", controller.adminSignup)
router.post("/admin/login",controller.adminLogin)
router.post("/admin/logout",auth.adminAuth,controller.adminLogOut)

router.get("/feedback",controller.getAllFeedback)

//getting items from admin
router.get('/adminItems/:category', controller.getAllItems);

router.delete("/adminItems/:id", auth.adminAuth, controller.deleteItem);

router.get("/stats", auth.adminAuth, controller.getStats)

module.exports = router;