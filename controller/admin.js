const adminModel = require("../model/adminModel")
const feedbackModel = require("../model/feedbackModel")
const itemModel = require("../model/itemModel")
const userModel = require("../model/userModel")


const adminSignup = async (req,res) => {
    try{
        const data = req.body
        const admin = new adminModel(data)

        await admin.save() // save to database  

        const authToken = await admin.generateAuthToken()

        res.status(200).send({data:data, token:authToken})
    }catch(e){
        res.status(400).send({message:e})
    }
}

const adminLogin = async(req, res) => {
    try{
        const data = req.body
        const admin = await adminModel.findByCredentials(data.email,data.password) // cross checking password and user existence
        const token = await admin.generateAuthToken()

        res.cookie("session_id",token)
        res.status(200).send({message:admin, loginToken:token})
    }catch(e){
        res.status(400).send({message:e})
    }
}

const adminLogOut = async (req, res) => {
    try{
        req.admin.tokens = req.admin.tokens.filter((token) => { // deleting token after signout
            return token.token !== req.token
        })

        await req.admin.save() // saving the new user data
        
        res.status(200).send({message:"Admin Logged out"})
    }catch(e){
        res.status(400).send({message:e})
    }
}

const getAllFeedback = async (req,res) => {
    try{
        const feedbacks = await feedbackModel.find({})
        res.status(200).send({message:feedbacks})
    }catch(errror) {
        res.status(400).send({message:"Unsuccessful"})
    }
} 
// Get all items
const getAllItems = async (req, res) => {
    try {
      const items = await itemModel.find({category:req.params.category});
      res.json(items);
    } catch (error) {
      res.status(400).json({ error: error.message });
    }
  };

const deleteItem = async (req, res) => {
    try {
      const orders = await itemModel.findOneAndDelete({ _id: req.params.id });
      res.status(200).send({ message: orders });
    } catch (e) {
      res.status(400).send({ message: "unsuccess" });
    }
  };
  
const getStats = async (req,res) => {
    try{
        const data = {
            // newUser:await userModel.countDocuments({ createdAt: { $gte: today } }),
            totalUser: await userModel.countDocuments(),
            totalFeedback: await feedbackModel.countDocuments(),
            totalLost: await itemModel.countDocuments({ category: 'lost' }),
            totalFound: await itemModel.countDocuments({category: 'found'})
        }
        res.status(200).send({message:data})
    }catch(e){
        res.status(400).send({message:e})
    }
}

module.exports = {adminSignup, adminLogOut, adminLogin,getAllFeedback, getAllItems, deleteItem, getStats}